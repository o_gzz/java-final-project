/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Code;

/**
 *
 * @author QW
 */
public class Department extends Location{
    protected int dept_id;
    private String name;
    private int manager_id;

    public Department() {
    }
    
    //Constructor utilizado cuando se tienen todos los datos
    public Department(int dept_id, String name, int manager_id, int location_id) {
        super(location_id);
        this.dept_id = dept_id;
        this.name = name;
        this.manager_id = manager_id;
    }

    public Department(int dept_id, int manager_id) {
        this.dept_id = dept_id;
        this.manager_id = manager_id;
    }

    public Department(int dept_id, String name, int manager_id) {
        this.dept_id = dept_id;
        this.name = name;
        this.manager_id = manager_id;
    }

    public Department(int dept_id) {
        this.dept_id = dept_id;
    }

    

    public int getDept_id() {
        return dept_id;
    }

    public void setDept_id(int dept_id) {
        this.dept_id = dept_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getManager_id() {
        return manager_id;
    }

    public void setManager_id(int manager_id) {
        this.manager_id = manager_id;
    }
    
}
