/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Code;

import java.util.Date;

/**
 *
 * @author QW
 */
public class Empleado extends Department{
    protected int employee_id;
    private String name;
    private String lastName;
    private String email;
    private long phone;
    private Date hire_date;
    private int job_id;
    private float salary;
    private float commission;
    

    public Empleado() {
    }

    public Empleado(int employee_id, String name, String lastName, String email, long phone, Date hire_date, int job_id, float salary, float commission, int manager_id,  int dept_id) {
        super(manager_id, dept_id);
        this.employee_id = employee_id;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.hire_date = hire_date;
        this.job_id = job_id;
        this.salary = salary;
        this.commission = commission;
    }    

    public int getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public Date getHire_date() {
        return hire_date;
    }

    public void setHire_date(Date hire_date) {
        this.hire_date = hire_date;
    }

    public int getJob_id() {
        return job_id;
    }

    public void setJob_id(int job_id) {
        this.job_id = job_id;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public float getCommission() {
        return commission;
    }

    public void setCommission(float commission) {
        this.commission = commission;
    }

    

    
}
