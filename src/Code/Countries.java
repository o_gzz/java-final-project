/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Code;

/**
 *
 * @author QW
 */
public class Countries {
    protected int country_id;
    private String name;
    private String region;

    public Countries(int country_id, String name, String region) {
        this.country_id = country_id;
        this.name = name;
        this.region = region;
    }

    public Countries(int country_id) {
        this.country_id = country_id;
    }

    public Countries() {
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
    
}
