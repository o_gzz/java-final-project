/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Code;

/**
 *
 * @author QW
 */
public class Location extends Countries{
    protected int location_id;
    private String st_address;
    private int postal_code;
    private String city;
    private String state;

    public Location(int location_id) {
        this.location_id = location_id;
    }

    public Location(int location_id, String st_address, int postal_code, String city, String state, int country_id) {
        super(country_id);
        this.location_id = location_id;
        this.st_address = st_address;
        this.postal_code = postal_code;
        this.city = city;
        this.state = state;
    }

    public Location(int location_id, String st_address, int postal_code, String city, String state) {
        this.location_id = location_id;
        this.st_address = st_address;
        this.postal_code = postal_code;
        this.city = city;
        this.state = state;
    }

    public Location() {
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public String getSt_address() {
        return st_address;
    }

    public void setSt_address(String st_address) {
        this.st_address = st_address;
    }

    public int getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(int postal_code) {
        this.postal_code = postal_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
    
   
}
