/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Code;

/**
 *
 * @author QW
 */
public class Jobs {
    protected int job_id;
    private String name;
    private float min_salary;
    private float max_salary;

    public Jobs(int job_id, String name, float min_salary, float max_salary) {
        this.job_id = job_id;
        this.name = name;
        this.min_salary = min_salary;
        this.max_salary = max_salary;
    }

    public Jobs(int job_id) {
        this.job_id = job_id;
    }

    public Jobs() {
    }

    public int getJob_id() {
        return job_id;
    }

    public void setJob_id(int job_id) {
        this.job_id = job_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getMin_salary() {
        return min_salary;
    }

    public void setMin_salary(float min_salary) {
        this.min_salary = min_salary;
    }

    public float getMax_salary() {
        return max_salary;
    }

    public void setMax_salary(float max_salary) {
        this.max_salary = max_salary;
    }
    
}
