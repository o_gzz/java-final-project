/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Code;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author QW
 */
public class JobsView extends javax.swing.JFrame {

    /**
     * Creates new form JobsView
     */
    
    ArrayList <Jobs> listaJ = new ArrayList<>(); 
    EmployeeView emp = new EmployeeView();
    int job;
    String name;
    float min_salary;
    float max_salary;
    public JobsView() {
        initComponents();
        this.setLocationRelativeTo(null);
        tableJob.addMouseListener(new MouseAdapter(){
            DefaultTableModel model = new DefaultTableModel();
            public void mouseClicked(MouseEvent arg0) {
                int i = tableJob.getSelectedRow();
                job = (int) (tableJob.getValueAt(i, 0));
                name = (tableJob.getValueAt(i, 1).toString());
                min_salary = (float) (tableJob.getValueAt(i, 2));
                max_salary = (float)(tableJob.getValueAt(i, 3));
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButtonMinimize = new javax.swing.JButton();
        jButtonClose = new javax.swing.JButton();
        jButtonRegresar = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabelNombre10 = new javax.swing.JLabel();
        jLabelNombre11 = new javax.swing.JLabel();
        jLabelNombre12 = new javax.swing.JLabel();
        jLabelNombre13 = new javax.swing.JLabel();
        txtJobID = new javax.swing.JFormattedTextField();
        txtJobName = new javax.swing.JTextField();
        txtMinSalary = new javax.swing.JFormattedTextField();
        txtMaxSalary = new javax.swing.JFormattedTextField();
        jButtonSaveJob = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableJob = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jButtonDelete = new javax.swing.JButton();
        jButtonSaveTable = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(79, 93, 117));
        jPanel1.setPreferredSize(new java.awt.Dimension(1200, 50));

        jButtonMinimize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/minus-4-xxl.png"))); // NOI18N
        jButtonMinimize.setBorderPainted(false);
        jButtonMinimize.setContentAreaFilled(false);
        jButtonMinimize.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButtonMinimize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonMinimizeActionPerformed(evt);
            }
        });

        jButtonClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/cancel-xxl.png"))); // NOI18N
        jButtonClose.setBorderPainted(false);
        jButtonClose.setContentAreaFilled(false);
        jButtonClose.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButtonClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCloseActionPerformed(evt);
            }
        });

        jButtonRegresar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/arrow-81-xxl.png"))); // NOI18N
        jButtonRegresar.setBorderPainted(false);
        jButtonRegresar.setContentAreaFilled(false);
        jButtonRegresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRegresarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jButtonRegresar, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonMinimize, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonClose, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonRegresar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonClose)
                    .addComponent(jButtonMinimize))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setForeground(new java.awt.Color(45, 49, 66));
        jTabbedPane1.setFont(new java.awt.Font("Gadugi", 0, 14)); // NOI18N

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setFont(new java.awt.Font("Gadugi", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(239, 131, 84));
        jLabel2.setText("Datos de trabajo");

        jLabelNombre10.setFont(new java.awt.Font("Gadugi", 0, 14)); // NOI18N
        jLabelNombre10.setForeground(new java.awt.Color(45, 49, 66));
        jLabelNombre10.setText("Trabajo ID");

        jLabelNombre11.setFont(new java.awt.Font("Gadugi", 0, 14)); // NOI18N
        jLabelNombre11.setForeground(new java.awt.Color(45, 49, 66));
        jLabelNombre11.setText("Nombe");

        jLabelNombre12.setFont(new java.awt.Font("Gadugi", 0, 14)); // NOI18N
        jLabelNombre12.setForeground(new java.awt.Color(45, 49, 66));
        jLabelNombre12.setText("Salario Minimo");

        jLabelNombre13.setFont(new java.awt.Font("Gadugi", 0, 14)); // NOI18N
        jLabelNombre13.setForeground(new java.awt.Color(45, 49, 66));
        jLabelNombre13.setText("Salario Maximo");

        txtJobID.setForeground(new java.awt.Color(45, 49, 66));
        txtJobID.setFont(new java.awt.Font("Gadugi", 0, 14)); // NOI18N
        txtJobID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtJobIDActionPerformed(evt);
            }
        });
        txtJobID.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtJobIDKeyTyped(evt);
            }
        });

        txtJobName.setFont(new java.awt.Font("Gadugi", 0, 14)); // NOI18N
        txtJobName.setForeground(new java.awt.Color(45, 49, 66));
        txtJobName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtJobNameKeyTyped(evt);
            }
        });

        txtMinSalary.setForeground(new java.awt.Color(45, 49, 66));
        txtMinSalary.setFont(new java.awt.Font("Gadugi", 0, 14)); // NOI18N
        txtMinSalary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMinSalaryActionPerformed(evt);
            }
        });
        txtMinSalary.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtMinSalaryKeyTyped(evt);
            }
        });

        txtMaxSalary.setForeground(new java.awt.Color(45, 49, 66));
        txtMaxSalary.setFont(new java.awt.Font("Gadugi", 0, 14)); // NOI18N
        txtMaxSalary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMaxSalaryActionPerformed(evt);
            }
        });
        txtMaxSalary.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtMaxSalaryKeyTyped(evt);
            }
        });

        jButtonSaveJob.setFont(new java.awt.Font("Gadugi", 1, 14)); // NOI18N
        jButtonSaveJob.setForeground(new java.awt.Color(45, 49, 66));
        jButtonSaveJob.setText("Guardar");
        jButtonSaveJob.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(45, 49, 66), 1, true));
        jButtonSaveJob.setContentAreaFilled(false);
        jButtonSaveJob.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButtonSaveJob.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveJobActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonSaveJob, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(57, 57, 57)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelNombre10)
                                    .addComponent(jLabelNombre11)
                                    .addComponent(jLabelNombre12)
                                    .addComponent(jLabelNombre13))
                                .addGap(56, 56, 56)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtMaxSalary, javax.swing.GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE)
                                    .addComponent(txtMinSalary, javax.swing.GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtJobID, javax.swing.GroupLayout.DEFAULT_SIZE, 322, Short.MAX_VALUE)
                                        .addComponent(txtJobName)))))))
                .addContainerGap(645, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabel2)
                .addGap(34, 34, 34)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNombre10)
                    .addComponent(txtJobID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNombre11)
                    .addComponent(txtJobName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNombre12)
                    .addComponent(txtMinSalary, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNombre13)
                    .addComponent(txtMaxSalary, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(73, 73, 73)
                .addComponent(jButtonSaveJob, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(478, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Agregar trabajo", jPanel2);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setForeground(new java.awt.Color(45, 49, 66));
        jPanel3.setFont(new java.awt.Font("Gadugi", 0, 14)); // NOI18N

        tableJob.setFont(new java.awt.Font("Gadugi", 0, 14)); // NOI18N
        tableJob.setForeground(new java.awt.Color(45, 49, 66));
        tableJob.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Trabajo ID", "Nombre", "Salario Minimo", "Salario Maximo"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableJob.setRowHeight(25);
        jScrollPane1.setViewportView(tableJob);

        jLabel3.setFont(new java.awt.Font("Gadugi", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(239, 131, 84));
        jLabel3.setText("Trabajos registrados");

        jLabel4.setFont(new java.awt.Font("Gadugi", 0, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(45, 49, 66));
        jLabel4.setText("Para eliminar un elemento, seleccione y luego presione el boton eliminar");

        jButtonDelete.setFont(new java.awt.Font("Gadugi", 1, 14)); // NOI18N
        jButtonDelete.setForeground(new java.awt.Color(45, 49, 66));
        jButtonDelete.setText("Eliminar");
        jButtonDelete.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(45, 49, 66)));
        jButtonDelete.setContentAreaFilled(false);
        jButtonDelete.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButtonDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteActionPerformed(evt);
            }
        });

        jButtonSaveTable.setFont(new java.awt.Font("Gadugi", 1, 14)); // NOI18N
        jButtonSaveTable.setForeground(new java.awt.Color(45, 49, 66));
        jButtonSaveTable.setText("Guardar tabla");
        jButtonSaveTable.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(45, 49, 66), 1, true));
        jButtonSaveTable.setContentAreaFilled(false);
        jButtonSaveTable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButtonSaveTable.setDefaultCapable(false);
        jButtonSaveTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveTableActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Gadugi", 0, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(45, 49, 66));
        jLabel5.setText("Presione guardar si desea guardar la tabla actual");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(75, 75, 75)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 597, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel4))
                        .addGap(103, 103, 103)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jButtonSaveTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonDelete, javax.swing.GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE))))
                .addContainerGap(583, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabel3)
                .addGap(50, 50, 50)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jButtonDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonSaveTable, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addContainerGap(418, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Buscar en trabajos", jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1260, Short.MAX_VALUE)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonMinimizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonMinimizeActionPerformed
        this.setState(HR. ICONIFIED);
    }//GEN-LAST:event_jButtonMinimizeActionPerformed

    private void jButtonCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCloseActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButtonCloseActionPerformed

    private void jButtonRegresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRegresarActionPerformed
        HR rec_humanos = new HR();
        rec_humanos.setVisible(true);        
        rec_humanos.pack();                 
        rec_humanos.setDefaultCloseOperation(DeptView.EXIT_ON_CLOSE);
        this.dispose(); 
    }//GEN-LAST:event_jButtonRegresarActionPerformed

    private void txtJobIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtJobIDActionPerformed
       
    }//GEN-LAST:event_txtJobIDActionPerformed

    private void txtMinSalaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMinSalaryActionPerformed
        
    }//GEN-LAST:event_txtMinSalaryActionPerformed

    private void txtMaxSalaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMaxSalaryActionPerformed
        
    }//GEN-LAST:event_txtMaxSalaryActionPerformed

    private void txtJobIDKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtJobIDKeyTyped
        emp.validarDigitos(evt);
    }//GEN-LAST:event_txtJobIDKeyTyped

    private void jButtonSaveJobActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveJobActionPerformed
        job = Integer.parseInt((String) txtJobID.getText());
        min_salary = Float.parseFloat(txtMinSalary.getText());
        max_salary = Float.parseFloat(txtMaxSalary.getText());
        Jobs nuevo_trabajo = new Jobs(job, txtJobName.getText(), min_salary, max_salary);
        listaJ.add(nuevo_trabajo);
        agregarTablaJob();
        JOptionPane.showMessageDialog(null, "Trabajo nuevo agregado");
        txtJobID.setText("");
        txtJobName.setText("");
        txtMinSalary.setText("");
        txtMaxSalary.setText("");
    }//GEN-LAST:event_jButtonSaveJobActionPerformed
    private void agregarTablaJob(){
        Object tabla_job [][] = new Object[listaJ.size()][4];
        for(int i=0; i<listaJ.size(); i++){
          tabla_job[i][0] = listaJ.get(i).getJob_id();
          tabla_job[i][1] = listaJ.get(i).getName();
          tabla_job[i][2] = listaJ.get(i).getMin_salary();
          tabla_job[i][3] = listaJ.get(i).getMax_salary();
        }
        tableJob.setModel(new javax.swing.table.DefaultTableModel(
            tabla_job,
            new String [] {
                "Trabajo ID", "Nombre", "Salario Minimo", "Salario Maximo"
            }
        ));
    }
    private void txtJobNameKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtJobNameKeyTyped
        emp.validarLetras(evt);
    }//GEN-LAST:event_txtJobNameKeyTyped

    private void txtMinSalaryKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMinSalaryKeyTyped
        emp.validarDigitos(evt);
    }//GEN-LAST:event_txtMinSalaryKeyTyped

    private void txtMaxSalaryKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMaxSalaryKeyTyped
        emp.validarDigitos(evt);
    }//GEN-LAST:event_txtMaxSalaryKeyTyped

    private void jButtonDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteActionPerformed
        for(int i=0; i<listaJ.size(); i++){
            if(job==(listaJ.get(i).getJob_id())){
                listaJ.remove(i);
            }
        }
        //Actualiza los datos de la tabla despues de eliminar algun elemento
        agregarTablaJob();
    }//GEN-LAST:event_jButtonDeleteActionPerformed

    private void jButtonSaveTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveTableActionPerformed
        try{
            FileWriter file_out = new FileWriter("C:/Users/QW/Documents/NetBeansProjects/Project/Files/TablaTrabajos.txt");
            BufferedWriter bw = new BufferedWriter(file_out);
            bw.write("Trabajo ID\tNombre\tSalario Minimo   Salario Maximo\n");
            for(int i=0; i<tableJob.getRowCount(); i++){
                for(int j=0; j<tableJob.getColumnCount(); j++){
                    bw.write(tableJob.getValueAt(i, j)+ "\t");
                }
                bw.newLine();
            }
            bw.close();
            file_out.close();
            JOptionPane.showMessageDialog(null, "Tabla Guardada");
        }catch (IOException e){
            JOptionPane.showMessageDialog(null, "Ocurrio un error...");
        }
    }//GEN-LAST:event_jButtonSaveTableActionPerformed
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JobsView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JobsView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JobsView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JobsView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JobsView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonClose;
    private javax.swing.JButton jButtonDelete;
    private javax.swing.JButton jButtonMinimize;
    private javax.swing.JButton jButtonRegresar;
    private javax.swing.JButton jButtonSaveJob;
    private javax.swing.JButton jButtonSaveTable;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabelNombre10;
    private javax.swing.JLabel jLabelNombre11;
    private javax.swing.JLabel jLabelNombre12;
    private javax.swing.JLabel jLabelNombre13;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable tableJob;
    private javax.swing.JFormattedTextField txtJobID;
    private javax.swing.JTextField txtJobName;
    private javax.swing.JFormattedTextField txtMaxSalary;
    private javax.swing.JFormattedTextField txtMinSalary;
    // End of variables declaration//GEN-END:variables
}
